TrainingPeaks Code Test - Sean Hreha
====================

Demo: http://tpcodetest.seanhreha.com/

Welcome to the code repository for the TrainingPeaks Code Test.

The relevant project files are contained within the app folder.

This project leverages a gulp build process to output the files for production, as well as aid in development with live reloading and on the fly compiling for styles.

NPM and bower are required to run this project locally. To run this project simply navigate to the project folder in terminal and run:

npm install & bower install

gulp serve

