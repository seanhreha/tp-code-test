$(document).ready(function(){

	$('.slick').slick({
		centerMode: true,
		centerPadding: '0',
		speed: 500,
		slidesToShow: 1,
		autoplay: true,
		pauseOnHover: false,
		arrows: false,
		autoplaySpeed: 4000,
	});

	$('.slick').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	 	$(this).addClass('sliding');
	});

	$('.slick').on('afterChange', function(event, slick, currentSlide, nextSlide){
	 	$(this).removeClass('sliding');
	});

});